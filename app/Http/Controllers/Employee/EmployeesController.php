<?php

namespace App\Http\Controllers\Employee;

use App\Model\User;
use App\Model\Employee;
use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Services\EmployeeService;
use App\Http\Controllers\Controller;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::getEmployee()
            ->paginate(5);

        return view('employee.index')
            ->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employee.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = USER::find($id)
            ->join('employee', 'employee.id', '=', 'users.id')
            ->where('users.id', '=', $id)
            ->select('users.id', 'users.email', 'employee.firstname', 'employee.lastname', 'employee.position', 'employee.office', 'employee.salary', 'employee.birthday')
            ->get();
        if (count($user) > 0) {
            return view('employee.edit', ['data' => $user]);
        } else {
            return redirect('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request, EmployeeService $employeeUpdateService, $id)
    {
        $employeeUpdateService->updateEmployee($request, $id);
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request, EmployeeService $employeeservice)
    {
        $employeeservice->store($request);
        return redirect('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);

        $employee->delete();
        return redirect('/');
    }
}
