<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password-confirm' => 'min:6',
            'firstname' => 'required|min:2|max:50',
            'lastname' => 'required|min:2|max:50',
            'salary' => 'required|numeric|max:10000000',
            'office' => 'required|min:2|max:50',
            'position' => 'required|min:2|max:50',
            'birthday' => 'required',
        ];
    }
}
