<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;
    
    protected $table = 'employee';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'firstname',
        'lastname',
        'position',
        'office',
        'salary',
        'birthday',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'id',
        // 'firstname',
        // 'lastname',
        // 'position',
        // 'office',
        // 'salary',
        // 'birthday',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $dates = [
        'birthday',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function users()
    {
        return $this->belongsTo('App\Model\User','id');
    }

    public static function getEmployee()
    {
        return self::orderBy('id', 'DESC');
    }

}

