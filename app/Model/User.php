<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $fillable = [
        // 'id',
        'email',
        // 'email_verified_at',
        'password',
        // 'remember_token',
        // 'created_at',
        // 'updated_at',
    ];

    protected $dates = [
        'email_verified_at',
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'id',
        // 'email',
        'email_verified_at',
        // 'password',
        'remember_token',
        'created_at',
        'updated_at',
    ];

    public function employee()
    {
        return $this->hasOne('App\Model\Employee', 'id');
    }
}
