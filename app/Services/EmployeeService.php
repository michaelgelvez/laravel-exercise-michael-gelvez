<?php
namespace App\Services;

use App\Model\Employee;
use App\Model\User;
use Illuminate\Support\Facades\Hash;

class EmployeeService
{
    public function store($input)
    {
        $input['userDetails'] = array_merge(
            [
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
            ]
        );

        $id = User::create($input['userDetails'])->id;

        $input['employeeDetails'] = array_merge(
            [
                'id' => $id,
                'firstname' => $input['firstname'],
                'lastname' => $input['lastname'],
                'office' => $input['office'],
                'salary' => $input['salary'],
                'position' => $input['position'],
                'birthday' => $input['birthday'],
            ]
        );

        Employee::create($input['employeeDetails']);
    }

    public function updateEmployee($request,$id)
    {
        $user = User::find($id);
        $user->email = $request->input('email');
        $user->save();

        $employee = Employee::find($id);
        $employee->firstname = $request->input('firstname');
        $employee->lastname = $request->input('lastname');
        $employee->position = $request->input('position');
        $employee->office = $request->input('office');
        $employee->salary = $request->input('salary');
        $employee->birthday = $request->input('birthday');

        $employee->save();
    }

}



