<header class="main-header" role="banner">
      		<img src="{{ asset('banner.png') }}" class="banner_image" alt="Banner Image"/>

      	<br>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">

         <img src="{{ asset('logo.png') }}" class="logo_image bg-light" alt="Banner Image"/>
         <button class="navbar-toggler ml-1" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link text-white" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item">
              <a class="nav-link text-white " href="{{ route('employee.create') }}">Add Employee</a>
            </li>

            <li class="nav-item">

            </li>
          </ul>

          @guest
              <a class="nav-link text-white" href="{{ route('login') }}">{{ __('Login') }}</a>
              @else
                <a class="nav-link text-white" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Welcome,
               {{ Auth::user()->first()->employee['firstname'] }} {{ Auth::user()->first()->employee['lastname'] }}
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>

              @endguest
          <form class="form-inline my-2 my-lg-0">

          </form>
        </div>


    </nav>
    </header>
