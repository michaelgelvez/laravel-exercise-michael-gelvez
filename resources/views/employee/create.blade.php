@extends('layouts.master')

@section('title','Insert Employee')

@section('content')
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label class="col-md"><h3>{{ __('Create Employee') }}</h3></label>
                        <label class="col-md">{{ __('Please Fill in this form to create an account.') }}</label>
                    </div>
                    <hr>
                    <form method="get" action="{{ route('employee.store') }}" id="add_form">
                        @csrf

                        <div class="form-group">
                            <label for="firstname" class="col-md-4 col-form-label">{{ __('First Name') }}</label>

                            <div class="col">
                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label for="lastname" class="col-md-4 col-form-label">{{ __('Last Name') }}</label>

                            <div class="col">
                                <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>

                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>



                            <label for="position" class="col-md-4 col-form-label">{{ __('Position') }}</label>

                            <div class="col">
                                <input id="position" type="text" class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}" name="position" value="{{ old('position') }}" required autofocus>

                                @if ($errors->has('position'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
                                @endif
                            </div>



                            <label for="salary" class="col-md-4 col-form-label">{{ __('Salary') }}</label>

                            <div class="col">
                                <input id="salary" type="text" class="form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}" name="salary" value="{{ old('salary') }}" required autofocus>

                                @if ($errors->has('salary'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('salary') }}</strong>
                                    </span>
                                @endif
                            </div>



                            <label for="office" class="col-md-4 col-form-label">{{ __('Office') }}</label>

                            <div class="col">
                                <input id="office" type="text" class="form-control{{ $errors->has('office') ? ' is-invalid' : '' }}" name="office" value="{{ old('office') }}" required autofocus>

                                @if ($errors->has('office'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('office') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>

                            <div class="col">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <label for="password-confirm" class="col-md-4 col-form-label">{{ __('Repeat Password') }}</label>

                            <div class="col">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>

                            <label for="birthday" class="col-md-4 col-form-label">{{ __('Birthday') }}</label>

                            <div class="col">
                                <input id="birthday" type="date" class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}" name="birthday" value="{{ old('birthday') }}" required autofocus>

                                @if ($errors->has('birthday'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-6">
                                <button type="submit" class="btn btn-danger btn-block reset_add">
                                    {{ __('Reset') }}
                                </button>

                            </div>
                            <div class="col-6">
                            <button type="submit" class="btn btn-success btn-block">
                                    {{ __('Add Employee') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection