@extends('layouts.master')

@section('title','Edit Employee')

@section('content')
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label class="col-md">
                            <h3>{{ __('Edit Employee') }}</h3>
                        </label>
                        <label class="col-md">{{ __('Please Fill in this form to edit an account.') }}</label>
                    </div>
                    <hr>
                    <form method="POST" action="{{ route('employee.update',$data[0]->id ) }}">
                        @csrf
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="id" value="{{  $data[0]->id   }}">
                        <div class="form-group">
                            <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}</label>

                            <div class="col">
                                <input id="firstname" type="text"
                                    class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}"
                                    name="firstname" value="{{ ($data) ? $data[0]->firstname : '' }}" required
                                    autofocus>

                                @if ($errors->has('firstname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('firstname') }}</strong>
                                </span>
                                @endif
                            </div>

                            <label for="lastname" class="col-md-4 col-form-label">{{ __('Last Name') }}</label>

                            <div class="col">
                                <input id="lastname" type="text"
                                    class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}"
                                    name="lastname" value="{{ ($data) ? $data[0]->lastname : '' }}" required autofocus>

                                @if ($errors->has('lastname'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lastname') }}</strong>
                                </span>
                                @endif
                            </div>

                            <label for="position" class="col-md-4 col-form-label">{{ __('Position') }}</label>

                            <div class="col">
                                <input id="position" type="text"
                                    class="form-control{{ $errors->has('position') ? ' is-invalid' : '' }}"
                                    name="position" value="{{ ($data) ? $data[0]->position : '' }}" required autofocus>

                                @if ($errors->has('position'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('position') }}</strong>
                                </span>
                                @endif
                            </div>



                            <label for="salary" class="col-md-4 col-form-label">{{ __('Salary') }}</label>

                            <div class="col">
                                <input id="salary" type="text"
                                    class="form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}" name="salary"
                                    value="{{ ($data) ? $data[0]->salary : '' }}" required autofocus>

                                @if ($errors->has('salary'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('salary') }}</strong>
                                </span>
                                @endif
                            </div>



                            <label for="office" class="col-md-4 col-form-label">{{ __('Office') }}</label>

                            <div class="col">
                                <input id="office" type="text"
                                    class="form-control{{ $errors->has('office') ? ' is-invalid' : '' }}" name="office"
                                    value="{{ ($data) ? $data[0]->office : '' }}" required autofocus>

                                @if ($errors->has('office'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('office') }}</strong>
                                </span>
                                @endif
                            </div>



                            <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col">
                                <input id="email" type="email"
                                    class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                    value="{{ ($data) ? $data[0]->email : '' }}" required>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <label for="birthday" class="col-md-4 col-form-label">{{ __('Birthday') }}</label>

                            <div class="col">
                                <input id="birthday" type="date"
                                    class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}"
                                    name="birthday" value="{{ ($data) ? $data[0]->birthday : '' }}" required autofocus>

                                @if ($errors->has('birthday'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('birthday') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-6 float-right">
                            <button type="submit" class="btn btn-success btn-block float">
                                {{ __('Update') }}
                            </button>
                        </div>

                    </form>
                    <div class="col-6">
                        <form action="{{ route('employee.destroy', $data[0]->id ) }}" method="get">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-block reset_edit">
                                {{ __('Delete') }}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(document).on('click', '.reset_add', function (e) {
        e.preventDefault();
        $("#add_form").trigger("reset");
    });

</script>
@endpush
