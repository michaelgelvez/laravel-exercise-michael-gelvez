@extends('layouts.master')

@section('title','Show Employee')

@section('content')
<div class="container">
    <h2>Employee</h2>
    <table class="table table-bordered" id="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Salary</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($employees as $user)
            <tr>
                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                <td>{{ $user->position }}</td>
                <td>{{ $user->office }}</td>
                <td>{{ $user->salary }}</td>
                <td><a href="{{ url('/employee/edit',$user->id )}}"><button><i class="fa fa-edit"></i></button></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@push('scripts')
<script>
    $(function () {
        $('#table').DataTable({
            processing: true,
            "bLengthChange": true,
            "searching": false,
            'columnDefs': [{
                    "targets": 1,
                    "className": "text-center",
                    "width": "20%"
                },
                {
                    "targets": 4,
                    "className": "text-center",
                    "width": "4%"
                }
            ]
        });
    });

</script>
@endpush
