<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('components.head')

<body>
    @include('components.header')

    @yield('content')

    {{-- @include('components.footer') --}}
    {{-- @include('partials.script') --}}
    @stack('scripts')
</body>

</html>