<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'Employee\EmployeesController@index');

Route::group(['prefix' => 'employee'], function (): void {

        Route::get('/create', [
            'as' => 'employee.create',
            'uses' => 'Employee\EmployeesController@create',
        ]);

        Route::get('/store', [
            'as' => 'employee.store',
            'uses' => 'Employee\EmployeesController@store',
        ]);

        Route::get('/destroy/{post}', [
            'as' => 'employee.destroy',
            'uses' => 'Employee\EmployeesController@destroy',
        ]);

        Route::get('/edit/{post}', [
            'as' => 'employee.edit',
            'uses' => 'Employee\EmployeesController@edit',
        ]);

        Route::patch('/update/{post}', [
            'as' => 'employee.update',
            'uses' => 'Employee\EmployeesController@update',
        ]);

});

Auth::routes(['register' => false]);
